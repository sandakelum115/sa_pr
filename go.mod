module sa_pr

go 1.13

require (
	github.com/go-sql-driver/mysql v1.6.0
	github.com/gorilla/mux v1.8.0
	github.com/gorilla/sessions v1.2.1
	golang.org/x/crypto v0.0.0-20211215153901-e495a2d5b3d3
)
