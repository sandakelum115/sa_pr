package database

import (
	"database/sql"
	"fmt"

	_ "github.com/go-sql-driver/mysql"
)

var DB *sql.DB

func Connect() {
	fmt.Println("start connecting")

	config := Config{
		Host:     "us-cdbr-east-05.cleardb.net",
		User:     "b8b666e2c26731",
		Password: "361f7e32",
		DB:       "heroku_b973fe3d7dba5f2",
	}
	var err error
	DB, err = sql.Open("mysql", config.User+":"+config.Password+"@tcp("+config.Host+":3306)/"+config.DB)
	if err != nil {
		panic(err.Error())
	}

	// defer DB.Close()

	fmt.Println("successfully connected :)")
}
