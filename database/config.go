package database

type Config struct {
	Host     string
	User     string
	Password string
	DB       string
}
