package user_pkg

import (
	"encoding/json"
	"fmt"
	"net/http"
	"os"
	"sa_pr/database"

	"github.com/gorilla/sessions"
	"golang.org/x/crypto/bcrypt"
)

type User struct {
	Id        int32  `json:"id"`
	Email     string `json:"email"`
	Password  string `json:"password"`
	FullName  string `json:"fullName"`
	Telephone string `json:"telephone"`
	Address   string `json:"address"`
}

var store = sessions.NewCookieStore([]byte(os.Getenv("SESSION_KEY")))

func (u *User) HashPassword() string {
	bytes, err := bcrypt.GenerateFromPassword([]byte(u.Password), 14)
	if err != nil {
		panic(err.Error())
	}
	return string(bytes)
}

func getUser(email string) User {
	q1 := fmt.Sprintf("SELECT email,fullName,password,telephone,address FROM users WHERE email = '%s'", email)
	var u User
	rows, err := database.DB.Query(q1)
	if err != nil {
		panic(err.Error())
	}

	for rows.Next() {
		rows.Scan(&u.Email, &u.FullName, &u.Password, &u.Telephone, &u.Address)
	}

	return u
}

func (u *User) createUser() bool {

	q1 := fmt.Sprintf("SELECT email FROM users WHERE email = '%s'", u.Email)
	rows, err := database.DB.Query(q1)
	count := 0
	for rows.Next() {
		count += 1
	}
	// println(count)
	if err != nil {
		panic(err.Error())
	}
	defer rows.Close()

	if count == 0 {

		q := fmt.Sprintf("INSERT INTO users (email,fullName,password,telephone,address)	 VALUES('%s','%s','%s','%s','%s')", u.Email, u.FullName, u.Password, u.Telephone, u.Address)
		insert, _ := database.DB.Query(q)
		defer insert.Close()
		return true
	} else {
		return false
	}

}

func (u *User) updateUser() {
	q := fmt.Sprintf("UPDATE users SET fullName='%s',telephone='%s',address='%s' WHERE email = '%s'", u.FullName, u.Telephone, u.Address, u.Email)
	update, err := database.DB.Query(q)

	if err != nil {
		panic(err.Error())
	}
	defer update.Close()
}

// route func
func Login(w http.ResponseWriter, r *http.Request) {

}

func SignUp(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	var u User
	json.NewDecoder(r.Body).Decode(&u)
	u.Password = u.HashPassword()

	isOk := u.createUser()

	if isOk {
		uJson, err := json.Marshal(u)
		if err != nil {
			panic(err.Error())
		}
		//save session
		session, _ := store.Get(r, "auth")
		session.Options = &sessions.Options{
			Path:     "/",
			MaxAge:   86400 * 7,
			HttpOnly: true,
		}

		session.Values["email"] = u.Email
		session.Save(r, w)

		fmt.Println(session)
		//
		fmt.Println(string(uJson))
		w.Write([]byte("1"))
	} else {
		w.Write([]byte("0"))
	}

	// fmt.Println(u)
}

func EditProfile(w http.ResponseWriter, r *http.Request) {
	session, _ := store.Get(r, "auth")
	// email := string(session.Values["email"])
	// fmt.Println(email)
	if session.Values["email"] != nil {
		if email, ok := session.Values["email"].(string); ok {
			fmt.Println(email)
			u := getUser(email)
			var newU User
			json.NewDecoder(r.Body).Decode(&newU)

			u.FullName = newU.FullName
			u.Telephone = newU.Telephone
			u.Address = newU.Address
			u.updateUser()

			json.NewEncoder(w).Encode(u)

		}

	} else {
		w.Write([]byte("0"))
	}

}

func Profile(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	session, _ := store.Get(r, "auth")
	if session.Values["email"] != nil {
		if email, ok := session.Values["email"].(string); ok {
			u := getUser(email)
			json.NewEncoder(w).Encode(u)
		}
	}

}
