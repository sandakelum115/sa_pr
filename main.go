package main

import (
	"fmt"
	"log"
	"net/http"
	"os"
	"sa_pr/database"
	"sa_pr/user_pkg"

	"github.com/gorilla/mux"
)

func main() {
	database.Connect()
	defer database.DB.Close()
	// os.Setenv("PORT", "8000")
	port := os.Getenv("PORT")
	//
	r := mux.NewRouter()
	r.HandleFunc("/login", user_pkg.Login).Methods("post")
	r.HandleFunc("/sign-up", user_pkg.SignUp).Methods("post")
	r.HandleFunc("/edit-profile", user_pkg.EditProfile).Methods("post")
	r.HandleFunc("/profile", user_pkg.Profile).Methods("GET")
	fmt.Println(port)
	log.Fatal(http.ListenAndServe(":"+port, r))

}
